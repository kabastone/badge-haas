import { IBadge } from 'app/shared/model//badge.model';

export interface IEmploye {
    id?: number;
    matricule?: string;
    photo?: string;
    nom?: string;
    prenom?: string;
    telephone?: string;
    nomService?: string;
    badge?: IBadge;
}

export class Employe implements IEmploye {
    constructor(
        public id?: number,
        public matricule?: string,
        public photo?: string,
        public nom?: string,
        public prenom?: string,
        public telephone?: string,
        public nomService?: string,
        public badge?: IBadge
    ) {}
}
