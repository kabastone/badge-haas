import { IDroit } from 'app/shared/model//droit.model';

export interface IUtilisateur {
    id?: number;
    nom?: string;
    prenom?: string;
    login?: string;
    password?: string;
    droits?: IDroit[];
}

export class Utilisateur implements IUtilisateur {
    constructor(
        public id?: number,
        public nom?: string,
        public prenom?: string,
        public login?: string,
        public password?: string,
        public droits?: IDroit[]
    ) {}
}
