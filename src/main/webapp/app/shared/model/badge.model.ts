import { Moment } from 'moment';
import { IAcces } from 'app/shared/model//acces.model';

export const enum TypeBadge {
    VISITEUR = 'VISITEUR',
    VISITEUR_ESCORTE = 'VISITEUR_ESCORTE',
    PELERIN = 'PELERIN',
    EMPLOYE = 'EMPLOYE',
    TEMPORAIRE = 'TEMPORAIRE'
}

export interface IBadge {
    id?: number;
    startDate?: Moment;
    endDate?: Moment;
    typeBadge?: TypeBadge;
    accces?: IAcces[];
}

export class Badge implements IBadge {
    constructor(
        public id?: number,
        public startDate?: Moment,
        public endDate?: Moment,
        public typeBadge?: TypeBadge,
        public accces?: IAcces[]
    ) {}
}
