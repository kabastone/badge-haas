export interface IDroit {
    id?: number;
    libelle?: string;
}

export class Droit implements IDroit {
    constructor(public id?: number, public libelle?: string) {}
}
