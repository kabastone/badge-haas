export interface ISecteur {
    id?: number;
    nom?: string;
    description?: string;
}

export class Secteur implements ISecteur {
    constructor(public id?: number, public nom?: string, public description?: string) {}
}
