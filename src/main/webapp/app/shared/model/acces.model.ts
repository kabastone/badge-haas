import { ISecteur } from 'app/shared/model//secteur.model';

export interface IAcces {
    id?: number;
    nom?: string;
    description?: string;
    secteurs?: ISecteur[];
}

export class Acces implements IAcces {
    constructor(public id?: number, public nom?: string, public description?: string, public secteurs?: ISecteur[]) {}
}
