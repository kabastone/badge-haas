import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BadgeAppAccesModule } from './acces/acces.module';
import { BadgeAppUtilisateurModule } from './utilisateur/utilisateur.module';
import { BadgeAppDroitModule } from './droit/droit.module';
import { BadgeAppSecteurModule } from './secteur/secteur.module';
import { BadgeAppEmployeModule } from './employe/employe.module';
import { BadgeAppBadgeModule } from './badge/badge.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        BadgeAppAccesModule,
        BadgeAppUtilisateurModule,
        BadgeAppDroitModule,
        BadgeAppSecteurModule,
        BadgeAppEmployeModule,
        BadgeAppBadgeModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BadgeAppEntityModule {}
