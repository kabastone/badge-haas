import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Acces } from 'app/shared/model/acces.model';
import { AccesService } from './acces.service';
import { AccesComponent } from './acces.component';
import { AccesDetailComponent } from './acces-detail.component';
import { AccesUpdateComponent } from './acces-update.component';
import { AccesDeletePopupComponent } from './acces-delete-dialog.component';
import { IAcces } from 'app/shared/model/acces.model';

@Injectable({ providedIn: 'root' })
export class AccesResolve implements Resolve<IAcces> {
    constructor(private service: AccesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Acces> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Acces>) => response.ok),
                map((acces: HttpResponse<Acces>) => acces.body)
            );
        }
        return of(new Acces());
    }
}

export const accesRoute: Routes = [
    {
        path: 'acces',
        component: AccesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.acces.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acces/:id/view',
        component: AccesDetailComponent,
        resolve: {
            acces: AccesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.acces.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acces/new',
        component: AccesUpdateComponent,
        resolve: {
            acces: AccesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.acces.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acces/:id/edit',
        component: AccesUpdateComponent,
        resolve: {
            acces: AccesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.acces.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const accesPopupRoute: Routes = [
    {
        path: 'acces/:id/delete',
        component: AccesDeletePopupComponent,
        resolve: {
            acces: AccesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.acces.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
