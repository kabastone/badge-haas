import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IAcces } from 'app/shared/model/acces.model';
import { AccesService } from './acces.service';
import { ISecteur } from 'app/shared/model/secteur.model';
import { SecteurService } from 'app/entities/secteur';

@Component({
    selector: 'jhi-acces-update',
    templateUrl: './acces-update.component.html'
})
export class AccesUpdateComponent implements OnInit {
    acces: IAcces;
    isSaving: boolean;

    secteurs: ISecteur[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected accesService: AccesService,
        protected secteurService: SecteurService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ acces }) => {
            this.acces = acces;
        });
        this.secteurService.query().subscribe(
            (res: HttpResponse<ISecteur[]>) => {
                this.secteurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.acces.id !== undefined) {
            this.subscribeToSaveResponse(this.accesService.update(this.acces));
        } else {
            this.subscribeToSaveResponse(this.accesService.create(this.acces));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAcces>>) {
        result.subscribe((res: HttpResponse<IAcces>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSecteurById(index: number, item: ISecteur) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
