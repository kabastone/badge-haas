export * from './acces.service';
export * from './acces-update.component';
export * from './acces-delete-dialog.component';
export * from './acces-detail.component';
export * from './acces.component';
export * from './acces.route';
