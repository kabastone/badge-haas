import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAcces } from 'app/shared/model/acces.model';

type EntityResponseType = HttpResponse<IAcces>;
type EntityArrayResponseType = HttpResponse<IAcces[]>;

@Injectable({ providedIn: 'root' })
export class AccesService {
    public resourceUrl = SERVER_API_URL + 'api/acces';

    constructor(protected http: HttpClient) {}

    create(acces: IAcces): Observable<EntityResponseType> {
        return this.http.post<IAcces>(this.resourceUrl, acces, { observe: 'response' });
    }

    update(acces: IAcces): Observable<EntityResponseType> {
        return this.http.put<IAcces>(this.resourceUrl, acces, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAcces>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAcces[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
