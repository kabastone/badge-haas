import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAcces } from 'app/shared/model/acces.model';

@Component({
    selector: 'jhi-acces-detail',
    templateUrl: './acces-detail.component.html'
})
export class AccesDetailComponent implements OnInit {
    acces: IAcces;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ acces }) => {
            this.acces = acces;
        });
    }

    previousState() {
        window.history.back();
    }
}
