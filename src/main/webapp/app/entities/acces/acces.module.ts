import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BadgeAppSharedModule } from 'app/shared';
import {
    AccesComponent,
    AccesDetailComponent,
    AccesUpdateComponent,
    AccesDeletePopupComponent,
    AccesDeleteDialogComponent,
    accesRoute,
    accesPopupRoute
} from './';

const ENTITY_STATES = [...accesRoute, ...accesPopupRoute];

@NgModule({
    imports: [BadgeAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [AccesComponent, AccesDetailComponent, AccesUpdateComponent, AccesDeleteDialogComponent, AccesDeletePopupComponent],
    entryComponents: [AccesComponent, AccesUpdateComponent, AccesDeleteDialogComponent, AccesDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BadgeAppAccesModule {}
