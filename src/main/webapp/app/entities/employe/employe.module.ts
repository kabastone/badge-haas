import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BadgeAppSharedModule } from 'app/shared';
import {
    EmployeComponent,
    EmployeDetailComponent,
    EmployeUpdateComponent,
    EmployeDeletePopupComponent,
    EmployeDeleteDialogComponent,
    employeRoute,
    employePopupRoute
} from './';
import {EmployePrintComponent} from './employe-print.component';
import { BadgeEmployeComponent } from './badge-employe/badge-employe.component';

const ENTITY_STATES = [...employeRoute, ...employePopupRoute];

@NgModule({
    imports: [BadgeAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        EmployeComponent,
        EmployeDetailComponent,
        EmployeUpdateComponent,
        EmployeDeleteDialogComponent,
        EmployeDeletePopupComponent,
        EmployePrintComponent,
        BadgeEmployeComponent,
    ],
    entryComponents: [EmployeComponent, EmployeUpdateComponent, EmployeDeleteDialogComponent, EmployeDeletePopupComponent, EmployePrintComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BadgeAppEmployeModule {}
