import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {IEmploye} from '../../shared/model/employe.model';
import {Subscription} from 'rxjs/index';
import {EmployeService} from './employe.service';

import { AccountService } from 'app/core';

@Component({
    selector: 'jhi-employe-print',
    templateUrl: './employe-print.component.html'
})
export class EmployePrintComponent implements OnInit, OnDestroy {
    employes: IEmploye[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected employeService: EmployeService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.employeService.query().subscribe(
            (res: HttpResponse<IEmploye[]>) => {
                this.employes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInEmployes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IEmploye) {
        return item.id;
    }

    registerChangeInEmployes() {
        this.eventSubscriber = this.eventManager.subscribe('employeListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
