import {Component, Input, OnInit} from '@angular/core';
import {IBadge} from '../../../shared/model/badge.model';
import {IEmploye} from '../../../shared/model/employe.model';
import {ISecteur} from '../../../shared/model/secteur.model';
import {BadgeService} from '../../badge';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService} from 'ng-jhipster';
import {IAcces} from '../../../shared/model/acces.model';
import {AccesService} from '../../acces';

@Component({
  selector: 'jhi-badge-employe',
  templateUrl: './badge-employe.component.html',
  styles: []
})
export class BadgeEmployeComponent implements OnInit {
    @Input() employe: IEmploye;
    badge: IBadge;
    secteurs = [];
    acces: IAcces[];
    secteur1D = [];
    label: string;
    secteurColorClass = false;
  constructor(protected badgeService: BadgeService,
              protected jhiAlertService: JhiAlertService,
              protected  accesService: AccesService) {
  }

  ngOnInit() {
      this.findBadge();
  }
  findBadge() {
      this.badgeService.find(this.employe.badge.id).subscribe(
          (res: HttpResponse<IBadge>) => {
              this.badge = res.body;
              this.acces = this.badge.accces;
              this.secteurs = this.acces.map(
                  ac => ac.secteurs
              );
              this.secteur1D = [].concat(...this.secteurs);
          },
          (res: HttpErrorResponse) => this.onError(res.message)
      );
  }
    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
