import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BadgeAppSharedModule } from 'app/shared';
import {
    DroitComponent,
    DroitDetailComponent,
    DroitUpdateComponent,
    DroitDeletePopupComponent,
    DroitDeleteDialogComponent,
    droitRoute,
    droitPopupRoute
} from './';

const ENTITY_STATES = [...droitRoute, ...droitPopupRoute];

@NgModule({
    imports: [BadgeAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [DroitComponent, DroitDetailComponent, DroitUpdateComponent, DroitDeleteDialogComponent, DroitDeletePopupComponent],
    entryComponents: [DroitComponent, DroitUpdateComponent, DroitDeleteDialogComponent, DroitDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BadgeAppDroitModule {}
