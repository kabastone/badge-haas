import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDroit } from 'app/shared/model/droit.model';
import { DroitService } from './droit.service';

@Component({
    selector: 'jhi-droit-update',
    templateUrl: './droit-update.component.html'
})
export class DroitUpdateComponent implements OnInit {
    droit: IDroit;
    isSaving: boolean;

    constructor(protected droitService: DroitService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ droit }) => {
            this.droit = droit;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.droit.id !== undefined) {
            this.subscribeToSaveResponse(this.droitService.update(this.droit));
        } else {
            this.subscribeToSaveResponse(this.droitService.create(this.droit));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IDroit>>) {
        result.subscribe((res: HttpResponse<IDroit>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
