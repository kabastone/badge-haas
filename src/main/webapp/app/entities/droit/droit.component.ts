import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDroit } from 'app/shared/model/droit.model';
import { AccountService } from 'app/core';
import { DroitService } from './droit.service';

@Component({
    selector: 'jhi-droit',
    templateUrl: './droit.component.html'
})
export class DroitComponent implements OnInit, OnDestroy {
    droits: IDroit[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected droitService: DroitService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.droitService.query().subscribe(
            (res: HttpResponse<IDroit[]>) => {
                this.droits = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDroits();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDroit) {
        return item.id;
    }

    registerChangeInDroits() {
        this.eventSubscriber = this.eventManager.subscribe('droitListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
