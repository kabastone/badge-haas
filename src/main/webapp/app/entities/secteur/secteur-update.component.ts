import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ISecteur } from 'app/shared/model/secteur.model';
import { SecteurService } from './secteur.service';

@Component({
    selector: 'jhi-secteur-update',
    templateUrl: './secteur-update.component.html'
})
export class SecteurUpdateComponent implements OnInit {
    secteur: ISecteur;
    isSaving: boolean;

    constructor(protected secteurService: SecteurService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ secteur }) => {
            this.secteur = secteur;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.secteur.id !== undefined) {
            this.subscribeToSaveResponse(this.secteurService.update(this.secteur));
        } else {
            this.subscribeToSaveResponse(this.secteurService.create(this.secteur));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISecteur>>) {
        result.subscribe((res: HttpResponse<ISecteur>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
