import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISecteur } from 'app/shared/model/secteur.model';
import { AccountService } from 'app/core';
import { SecteurService } from './secteur.service';

@Component({
    selector: 'jhi-secteur',
    templateUrl: './secteur.component.html'
})
export class SecteurComponent implements OnInit, OnDestroy {
    secteurs: ISecteur[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected secteurService: SecteurService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.secteurService.query().subscribe(
            (res: HttpResponse<ISecteur[]>) => {
                this.secteurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSecteurs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISecteur) {
        return item.id;
    }

    registerChangeInSecteurs() {
        this.eventSubscriber = this.eventManager.subscribe('secteurListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
