import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BadgeAppSharedModule } from 'app/shared';
import {
    SecteurComponent,
    SecteurDetailComponent,
    SecteurUpdateComponent,
    SecteurDeletePopupComponent,
    SecteurDeleteDialogComponent,
    secteurRoute,
    secteurPopupRoute
} from './';

const ENTITY_STATES = [...secteurRoute, ...secteurPopupRoute];

@NgModule({
    imports: [BadgeAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SecteurComponent,
        SecteurDetailComponent,
        SecteurUpdateComponent,
        SecteurDeleteDialogComponent,
        SecteurDeletePopupComponent
    ],
    entryComponents: [SecteurComponent, SecteurUpdateComponent, SecteurDeleteDialogComponent, SecteurDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BadgeAppSecteurModule {}
