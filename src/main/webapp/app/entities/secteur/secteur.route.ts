import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Secteur } from 'app/shared/model/secteur.model';
import { SecteurService } from './secteur.service';
import { SecteurComponent } from './secteur.component';
import { SecteurDetailComponent } from './secteur-detail.component';
import { SecteurUpdateComponent } from './secteur-update.component';
import { SecteurDeletePopupComponent } from './secteur-delete-dialog.component';
import { ISecteur } from 'app/shared/model/secteur.model';

@Injectable({ providedIn: 'root' })
export class SecteurResolve implements Resolve<ISecteur> {
    constructor(private service: SecteurService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Secteur> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Secteur>) => response.ok),
                map((secteur: HttpResponse<Secteur>) => secteur.body)
            );
        }
        return of(new Secteur());
    }
}

export const secteurRoute: Routes = [
    {
        path: 'secteur',
        component: SecteurComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.secteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'secteur/:id/view',
        component: SecteurDetailComponent,
        resolve: {
            secteur: SecteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.secteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'secteur/new',
        component: SecteurUpdateComponent,
        resolve: {
            secteur: SecteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.secteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'secteur/:id/edit',
        component: SecteurUpdateComponent,
        resolve: {
            secteur: SecteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.secteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const secteurPopupRoute: Routes = [
    {
        path: 'secteur/:id/delete',
        component: SecteurDeletePopupComponent,
        resolve: {
            secteur: SecteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'badgeApp.secteur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
