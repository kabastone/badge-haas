package sn.haas.badge.repository;

import sn.haas.badge.domain.Badge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Badge entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BadgeRepository extends JpaRepository<Badge, Long> {

    @Query(value = "select distinct badge from Badge badge left join fetch badge.accces",
        countQuery = "select count(distinct badge) from Badge badge")
    Page<Badge> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct badge from Badge badge left join fetch badge.accces")
    List<Badge> findAllWithEagerRelationships();

    @Query("select badge from Badge badge left join fetch badge.accces where badge.id =:id")
    Optional<Badge> findOneWithEagerRelationships(@Param("id") Long id);

}
