package sn.haas.badge.repository;

import sn.haas.badge.domain.Acces;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Acces entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccesRepository extends JpaRepository<Acces, Long> {

    @Query(value = "select distinct acces from Acces acces left join fetch acces.secteurs",
        countQuery = "select count(distinct acces) from Acces acces")
    Page<Acces> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct acces from Acces acces left join fetch acces.secteurs")
    List<Acces> findAllWithEagerRelationships();

    @Query("select acces from Acces acces left join fetch acces.secteurs where acces.id =:id")
    Optional<Acces> findOneWithEagerRelationships(@Param("id") Long id);

}
