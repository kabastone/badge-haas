/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.haas.badge.web.rest.vm;
