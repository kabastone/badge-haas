package sn.haas.badge.web.rest;

import com.codahale.metrics.annotation.Timed;
import sn.haas.badge.domain.Acces;
import sn.haas.badge.repository.AccesRepository;
import sn.haas.badge.web.rest.errors.BadRequestAlertException;
import sn.haas.badge.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Acces.
 */
@RestController
@RequestMapping("/api")
public class AccesResource {

    private final Logger log = LoggerFactory.getLogger(AccesResource.class);

    private static final String ENTITY_NAME = "acces";

    private final AccesRepository accesRepository;

    public AccesResource(AccesRepository accesRepository) {
        this.accesRepository = accesRepository;
    }

    /**
     * POST  /acces : Create a new acces.
     *
     * @param acces the acces to create
     * @return the ResponseEntity with status 201 (Created) and with body the new acces, or with status 400 (Bad Request) if the acces has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/acces")
    @Timed
    public ResponseEntity<Acces> createAcces(@RequestBody Acces acces) throws URISyntaxException {
        log.debug("REST request to save Acces : {}", acces);
        if (acces.getId() != null) {
            throw new BadRequestAlertException("A new acces cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Acces result = accesRepository.save(acces);
        return ResponseEntity.created(new URI("/api/acces/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /acces : Updates an existing acces.
     *
     * @param acces the acces to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated acces,
     * or with status 400 (Bad Request) if the acces is not valid,
     * or with status 500 (Internal Server Error) if the acces couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/acces")
    @Timed
    public ResponseEntity<Acces> updateAcces(@RequestBody Acces acces) throws URISyntaxException {
        log.debug("REST request to update Acces : {}", acces);
        if (acces.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Acces result = accesRepository.save(acces);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, acces.getId().toString()))
            .body(result);
    }

    /**
     * GET  /acces : get all the acces.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of acces in body
     */
    @GetMapping("/acces")
    @Timed
    public List<Acces> getAllAcces(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Acces");
        return accesRepository.findAllWithEagerRelationships();
    }

    /**
     * GET  /acces/:id : get the "id" acces.
     *
     * @param id the id of the acces to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the acces, or with status 404 (Not Found)
     */
    @GetMapping("/acces/{id}")
    @Timed
    public ResponseEntity<Acces> getAcces(@PathVariable Long id) {
        log.debug("REST request to get Acces : {}", id);
        Optional<Acces> acces = accesRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(acces);
    }

    /**
     * DELETE  /acces/:id : delete the "id" acces.
     *
     * @param id the id of the acces to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/acces/{id}")
    @Timed
    public ResponseEntity<Void> deleteAcces(@PathVariable Long id) {
        log.debug("REST request to delete Acces : {}", id);

        accesRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
