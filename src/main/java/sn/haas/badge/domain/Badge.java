package sn.haas.badge.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import sn.haas.badge.domain.enumeration.TypeBadge;

/**
 * A Badge.
 */
@Entity
@Table(name = "badge")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Badge implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_badge")
    private TypeBadge typeBadge;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "badge_accces",
               joinColumns = @JoinColumn(name = "badges_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "accces_id", referencedColumnName = "id"))
    private Set<Acces> accces = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Badge startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Badge endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public TypeBadge getTypeBadge() {
        return typeBadge;
    }

    public Badge typeBadge(TypeBadge typeBadge) {
        this.typeBadge = typeBadge;
        return this;
    }

    public void setTypeBadge(TypeBadge typeBadge) {
        this.typeBadge = typeBadge;
    }

    public Set<Acces> getAccces() {
        return accces;
    }

    public Badge accces(Set<Acces> acces) {
        this.accces = acces;
        return this;
    }

    public Badge addAccces(Acces acces) {
        this.accces.add(acces);
        return this;
    }

    public Badge removeAccces(Acces acces) {
        this.accces.remove(acces);
        return this;
    }

    public void setAccces(Set<Acces> acces) {
        this.accces = acces;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Badge badge = (Badge) o;
        if (badge.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), badge.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Badge{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", typeBadge='" + getTypeBadge() + "'" +
            "}";
    }
}
