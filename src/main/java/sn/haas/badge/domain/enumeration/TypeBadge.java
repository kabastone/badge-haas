package sn.haas.badge.domain.enumeration;

/**
 * The TypeBadge enumeration.
 */
public enum TypeBadge {
    VISITEUR, VISITEUR_ESCORTE, PELERIN, EMPLOYE, TEMPORAIRE
}
