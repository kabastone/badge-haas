package sn.haas.badge.web.rest;

import sn.haas.badge.BadgeApp;

import sn.haas.badge.domain.Acces;
import sn.haas.badge.repository.AccesRepository;
import sn.haas.badge.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static sn.haas.badge.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AccesResource REST controller.
 *
 * @see AccesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BadgeApp.class)
public class AccesResourceIntTest {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private AccesRepository accesRepository;

    @Mock
    private AccesRepository accesRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAccesMockMvc;

    private Acces acces;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AccesResource accesResource = new AccesResource(accesRepository);
        this.restAccesMockMvc = MockMvcBuilders.standaloneSetup(accesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Acces createEntity(EntityManager em) {
        Acces acces = new Acces()
            .nom(DEFAULT_NOM)
            .description(DEFAULT_DESCRIPTION);
        return acces;
    }

    @Before
    public void initTest() {
        acces = createEntity(em);
    }

    @Test
    @Transactional
    public void createAcces() throws Exception {
        int databaseSizeBeforeCreate = accesRepository.findAll().size();

        // Create the Acces
        restAccesMockMvc.perform(post("/api/acces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(acces)))
            .andExpect(status().isCreated());

        // Validate the Acces in the database
        List<Acces> accesList = accesRepository.findAll();
        assertThat(accesList).hasSize(databaseSizeBeforeCreate + 1);
        Acces testAcces = accesList.get(accesList.size() - 1);
        assertThat(testAcces.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testAcces.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createAccesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accesRepository.findAll().size();

        // Create the Acces with an existing ID
        acces.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccesMockMvc.perform(post("/api/acces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(acces)))
            .andExpect(status().isBadRequest());

        // Validate the Acces in the database
        List<Acces> accesList = accesRepository.findAll();
        assertThat(accesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAcces() throws Exception {
        // Initialize the database
        accesRepository.saveAndFlush(acces);

        // Get all the accesList
        restAccesMockMvc.perform(get("/api/acces?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(acces.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllAccesWithEagerRelationshipsIsEnabled() throws Exception {
        AccesResource accesResource = new AccesResource(accesRepositoryMock);
        when(accesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restAccesMockMvc = MockMvcBuilders.standaloneSetup(accesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAccesMockMvc.perform(get("/api/acces?eagerload=true"))
        .andExpect(status().isOk());

        verify(accesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllAccesWithEagerRelationshipsIsNotEnabled() throws Exception {
        AccesResource accesResource = new AccesResource(accesRepositoryMock);
            when(accesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restAccesMockMvc = MockMvcBuilders.standaloneSetup(accesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restAccesMockMvc.perform(get("/api/acces?eagerload=true"))
        .andExpect(status().isOk());

            verify(accesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getAcces() throws Exception {
        // Initialize the database
        accesRepository.saveAndFlush(acces);

        // Get the acces
        restAccesMockMvc.perform(get("/api/acces/{id}", acces.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(acces.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAcces() throws Exception {
        // Get the acces
        restAccesMockMvc.perform(get("/api/acces/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAcces() throws Exception {
        // Initialize the database
        accesRepository.saveAndFlush(acces);

        int databaseSizeBeforeUpdate = accesRepository.findAll().size();

        // Update the acces
        Acces updatedAcces = accesRepository.findById(acces.getId()).get();
        // Disconnect from session so that the updates on updatedAcces are not directly saved in db
        em.detach(updatedAcces);
        updatedAcces
            .nom(UPDATED_NOM)
            .description(UPDATED_DESCRIPTION);

        restAccesMockMvc.perform(put("/api/acces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAcces)))
            .andExpect(status().isOk());

        // Validate the Acces in the database
        List<Acces> accesList = accesRepository.findAll();
        assertThat(accesList).hasSize(databaseSizeBeforeUpdate);
        Acces testAcces = accesList.get(accesList.size() - 1);
        assertThat(testAcces.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testAcces.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingAcces() throws Exception {
        int databaseSizeBeforeUpdate = accesRepository.findAll().size();

        // Create the Acces

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccesMockMvc.perform(put("/api/acces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(acces)))
            .andExpect(status().isBadRequest());

        // Validate the Acces in the database
        List<Acces> accesList = accesRepository.findAll();
        assertThat(accesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAcces() throws Exception {
        // Initialize the database
        accesRepository.saveAndFlush(acces);

        int databaseSizeBeforeDelete = accesRepository.findAll().size();

        // Get the acces
        restAccesMockMvc.perform(delete("/api/acces/{id}", acces.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Acces> accesList = accesRepository.findAll();
        assertThat(accesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Acces.class);
        Acces acces1 = new Acces();
        acces1.setId(1L);
        Acces acces2 = new Acces();
        acces2.setId(acces1.getId());
        assertThat(acces1).isEqualTo(acces2);
        acces2.setId(2L);
        assertThat(acces1).isNotEqualTo(acces2);
        acces1.setId(null);
        assertThat(acces1).isNotEqualTo(acces2);
    }
}
