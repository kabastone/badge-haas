import { element, by, ElementFinder } from 'protractor';

export class BadgeComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-badge div table .btn-danger'));
    title = element.all(by.css('jhi-badge div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BadgeUpdatePage {
    pageTitle = element(by.id('jhi-badge-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    startDateInput = element(by.id('field_startDate'));
    endDateInput = element(by.id('field_endDate'));
    typeBadgeSelect = element(by.id('field_typeBadge'));
    acccesSelect = element(by.id('field_accces'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setStartDateInput(startDate) {
        await this.startDateInput.sendKeys(startDate);
    }

    async getStartDateInput() {
        return this.startDateInput.getAttribute('value');
    }

    async setEndDateInput(endDate) {
        await this.endDateInput.sendKeys(endDate);
    }

    async getEndDateInput() {
        return this.endDateInput.getAttribute('value');
    }

    async setTypeBadgeSelect(typeBadge) {
        await this.typeBadgeSelect.sendKeys(typeBadge);
    }

    async getTypeBadgeSelect() {
        return this.typeBadgeSelect.element(by.css('option:checked')).getText();
    }

    async typeBadgeSelectLastOption() {
        await this.typeBadgeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async acccesSelectLastOption() {
        await this.acccesSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async acccesSelectOption(option) {
        await this.acccesSelect.sendKeys(option);
    }

    getAcccesSelect(): ElementFinder {
        return this.acccesSelect;
    }

    async getAcccesSelectedOption() {
        return this.acccesSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class BadgeDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-badge-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-badge'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
