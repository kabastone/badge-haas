/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DroitComponentsPage, DroitDeleteDialog, DroitUpdatePage } from './droit.page-object';

const expect = chai.expect;

describe('Droit e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let droitUpdatePage: DroitUpdatePage;
    let droitComponentsPage: DroitComponentsPage;
    let droitDeleteDialog: DroitDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Droits', async () => {
        await navBarPage.goToEntity('droit');
        droitComponentsPage = new DroitComponentsPage();
        expect(await droitComponentsPage.getTitle()).to.eq('badgeApp.droit.home.title');
    });

    it('should load create Droit page', async () => {
        await droitComponentsPage.clickOnCreateButton();
        droitUpdatePage = new DroitUpdatePage();
        expect(await droitUpdatePage.getPageTitle()).to.eq('badgeApp.droit.home.createOrEditLabel');
        await droitUpdatePage.cancel();
    });

    it('should create and save Droits', async () => {
        const nbButtonsBeforeCreate = await droitComponentsPage.countDeleteButtons();

        await droitComponentsPage.clickOnCreateButton();
        await promise.all([droitUpdatePage.setLibelleInput('libelle')]);
        expect(await droitUpdatePage.getLibelleInput()).to.eq('libelle');
        await droitUpdatePage.save();
        expect(await droitUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await droitComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Droit', async () => {
        const nbButtonsBeforeDelete = await droitComponentsPage.countDeleteButtons();
        await droitComponentsPage.clickOnLastDeleteButton();

        droitDeleteDialog = new DroitDeleteDialog();
        expect(await droitDeleteDialog.getDialogTitle()).to.eq('badgeApp.droit.delete.question');
        await droitDeleteDialog.clickOnConfirmButton();

        expect(await droitComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
