/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SecteurComponentsPage, SecteurDeleteDialog, SecteurUpdatePage } from './secteur.page-object';

const expect = chai.expect;

describe('Secteur e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let secteurUpdatePage: SecteurUpdatePage;
    let secteurComponentsPage: SecteurComponentsPage;
    let secteurDeleteDialog: SecteurDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Secteurs', async () => {
        await navBarPage.goToEntity('secteur');
        secteurComponentsPage = new SecteurComponentsPage();
        expect(await secteurComponentsPage.getTitle()).to.eq('badgeApp.secteur.home.title');
    });

    it('should load create Secteur page', async () => {
        await secteurComponentsPage.clickOnCreateButton();
        secteurUpdatePage = new SecteurUpdatePage();
        expect(await secteurUpdatePage.getPageTitle()).to.eq('badgeApp.secteur.home.createOrEditLabel');
        await secteurUpdatePage.cancel();
    });

    it('should create and save Secteurs', async () => {
        const nbButtonsBeforeCreate = await secteurComponentsPage.countDeleteButtons();

        await secteurComponentsPage.clickOnCreateButton();
        await promise.all([secteurUpdatePage.setNomInput('nom'), secteurUpdatePage.setDescriptionInput('description')]);
        expect(await secteurUpdatePage.getNomInput()).to.eq('nom');
        expect(await secteurUpdatePage.getDescriptionInput()).to.eq('description');
        await secteurUpdatePage.save();
        expect(await secteurUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await secteurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Secteur', async () => {
        const nbButtonsBeforeDelete = await secteurComponentsPage.countDeleteButtons();
        await secteurComponentsPage.clickOnLastDeleteButton();

        secteurDeleteDialog = new SecteurDeleteDialog();
        expect(await secteurDeleteDialog.getDialogTitle()).to.eq('badgeApp.secteur.delete.question');
        await secteurDeleteDialog.clickOnConfirmButton();

        expect(await secteurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
