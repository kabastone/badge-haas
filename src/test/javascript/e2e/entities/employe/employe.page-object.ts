import { element, by, ElementFinder } from 'protractor';

export class EmployeComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-employe div table .btn-danger'));
    title = element.all(by.css('jhi-employe div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class EmployeUpdatePage {
    pageTitle = element(by.id('jhi-employe-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    matriculeInput = element(by.id('field_matricule'));
    photoInput = element(by.id('field_photo'));
    nomInput = element(by.id('field_nom'));
    prenomInput = element(by.id('field_prenom'));
    telephoneInput = element(by.id('field_telephone'));
    nomServiceInput = element(by.id('field_nomService'));
    badgeSelect = element(by.id('field_badge'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setMatriculeInput(matricule) {
        await this.matriculeInput.sendKeys(matricule);
    }

    async getMatriculeInput() {
        return this.matriculeInput.getAttribute('value');
    }

    async setPhotoInput(photo) {
        await this.photoInput.sendKeys(photo);
    }

    async getPhotoInput() {
        return this.photoInput.getAttribute('value');
    }

    async setNomInput(nom) {
        await this.nomInput.sendKeys(nom);
    }

    async getNomInput() {
        return this.nomInput.getAttribute('value');
    }

    async setPrenomInput(prenom) {
        await this.prenomInput.sendKeys(prenom);
    }

    async getPrenomInput() {
        return this.prenomInput.getAttribute('value');
    }

    async setTelephoneInput(telephone) {
        await this.telephoneInput.sendKeys(telephone);
    }

    async getTelephoneInput() {
        return this.telephoneInput.getAttribute('value');
    }

    async setNomServiceInput(nomService) {
        await this.nomServiceInput.sendKeys(nomService);
    }

    async getNomServiceInput() {
        return this.nomServiceInput.getAttribute('value');
    }

    async badgeSelectLastOption() {
        await this.badgeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async badgeSelectOption(option) {
        await this.badgeSelect.sendKeys(option);
    }

    getBadgeSelect(): ElementFinder {
        return this.badgeSelect;
    }

    async getBadgeSelectedOption() {
        return this.badgeSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class EmployeDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-employe-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-employe'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
