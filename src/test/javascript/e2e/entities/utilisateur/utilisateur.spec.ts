/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UtilisateurComponentsPage, UtilisateurDeleteDialog, UtilisateurUpdatePage } from './utilisateur.page-object';

const expect = chai.expect;

describe('Utilisateur e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let utilisateurUpdatePage: UtilisateurUpdatePage;
    let utilisateurComponentsPage: UtilisateurComponentsPage;
    let utilisateurDeleteDialog: UtilisateurDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Utilisateurs', async () => {
        await navBarPage.goToEntity('utilisateur');
        utilisateurComponentsPage = new UtilisateurComponentsPage();
        expect(await utilisateurComponentsPage.getTitle()).to.eq('badgeApp.utilisateur.home.title');
    });

    it('should load create Utilisateur page', async () => {
        await utilisateurComponentsPage.clickOnCreateButton();
        utilisateurUpdatePage = new UtilisateurUpdatePage();
        expect(await utilisateurUpdatePage.getPageTitle()).to.eq('badgeApp.utilisateur.home.createOrEditLabel');
        await utilisateurUpdatePage.cancel();
    });

    it('should create and save Utilisateurs', async () => {
        const nbButtonsBeforeCreate = await utilisateurComponentsPage.countDeleteButtons();

        await utilisateurComponentsPage.clickOnCreateButton();
        await promise.all([
            utilisateurUpdatePage.setNomInput('nom'),
            utilisateurUpdatePage.setPrenomInput('prenom'),
            utilisateurUpdatePage.setLoginInput('login'),
            utilisateurUpdatePage.setPasswordInput('password')
            // utilisateurUpdatePage.droitSelectLastOption(),
        ]);
        expect(await utilisateurUpdatePage.getNomInput()).to.eq('nom');
        expect(await utilisateurUpdatePage.getPrenomInput()).to.eq('prenom');
        expect(await utilisateurUpdatePage.getLoginInput()).to.eq('login');
        expect(await utilisateurUpdatePage.getPasswordInput()).to.eq('password');
        await utilisateurUpdatePage.save();
        expect(await utilisateurUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await utilisateurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Utilisateur', async () => {
        const nbButtonsBeforeDelete = await utilisateurComponentsPage.countDeleteButtons();
        await utilisateurComponentsPage.clickOnLastDeleteButton();

        utilisateurDeleteDialog = new UtilisateurDeleteDialog();
        expect(await utilisateurDeleteDialog.getDialogTitle()).to.eq('badgeApp.utilisateur.delete.question');
        await utilisateurDeleteDialog.clickOnConfirmButton();

        expect(await utilisateurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
