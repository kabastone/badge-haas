/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AccesComponentsPage, AccesDeleteDialog, AccesUpdatePage } from './acces.page-object';

const expect = chai.expect;

describe('Acces e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let accesUpdatePage: AccesUpdatePage;
    let accesComponentsPage: AccesComponentsPage;
    let accesDeleteDialog: AccesDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Acces', async () => {
        await navBarPage.goToEntity('acces');
        accesComponentsPage = new AccesComponentsPage();
        expect(await accesComponentsPage.getTitle()).to.eq('badgeApp.acces.home.title');
    });

    it('should load create Acces page', async () => {
        await accesComponentsPage.clickOnCreateButton();
        accesUpdatePage = new AccesUpdatePage();
        expect(await accesUpdatePage.getPageTitle()).to.eq('badgeApp.acces.home.createOrEditLabel');
        await accesUpdatePage.cancel();
    });

    it('should create and save Acces', async () => {
        const nbButtonsBeforeCreate = await accesComponentsPage.countDeleteButtons();

        await accesComponentsPage.clickOnCreateButton();
        await promise.all([
            accesUpdatePage.setNomInput('nom'),
            accesUpdatePage.setDescriptionInput('description')
            // accesUpdatePage.secteurSelectLastOption(),
        ]);
        expect(await accesUpdatePage.getNomInput()).to.eq('nom');
        expect(await accesUpdatePage.getDescriptionInput()).to.eq('description');
        await accesUpdatePage.save();
        expect(await accesUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await accesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Acces', async () => {
        const nbButtonsBeforeDelete = await accesComponentsPage.countDeleteButtons();
        await accesComponentsPage.clickOnLastDeleteButton();

        accesDeleteDialog = new AccesDeleteDialog();
        expect(await accesDeleteDialog.getDialogTitle()).to.eq('badgeApp.acces.delete.question');
        await accesDeleteDialog.clickOnConfirmButton();

        expect(await accesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
