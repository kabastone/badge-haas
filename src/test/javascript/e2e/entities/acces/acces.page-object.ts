import { element, by, ElementFinder } from 'protractor';

export class AccesComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-acces div table .btn-danger'));
    title = element.all(by.css('jhi-acces div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AccesUpdatePage {
    pageTitle = element(by.id('jhi-acces-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nomInput = element(by.id('field_nom'));
    descriptionInput = element(by.id('field_description'));
    secteurSelect = element(by.id('field_secteur'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNomInput(nom) {
        await this.nomInput.sendKeys(nom);
    }

    async getNomInput() {
        return this.nomInput.getAttribute('value');
    }

    async setDescriptionInput(description) {
        await this.descriptionInput.sendKeys(description);
    }

    async getDescriptionInput() {
        return this.descriptionInput.getAttribute('value');
    }

    async secteurSelectLastOption() {
        await this.secteurSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async secteurSelectOption(option) {
        await this.secteurSelect.sendKeys(option);
    }

    getSecteurSelect(): ElementFinder {
        return this.secteurSelect;
    }

    async getSecteurSelectedOption() {
        return this.secteurSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AccesDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-acces-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-acces'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
