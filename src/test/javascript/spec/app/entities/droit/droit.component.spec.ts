/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BadgeAppTestModule } from '../../../test.module';
import { DroitComponent } from 'app/entities/droit/droit.component';
import { DroitService } from 'app/entities/droit/droit.service';
import { Droit } from 'app/shared/model/droit.model';

describe('Component Tests', () => {
    describe('Droit Management Component', () => {
        let comp: DroitComponent;
        let fixture: ComponentFixture<DroitComponent>;
        let service: DroitService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BadgeAppTestModule],
                declarations: [DroitComponent],
                providers: []
            })
                .overrideTemplate(DroitComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DroitComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DroitService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Droit(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.droits[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
