/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BadgeAppTestModule } from '../../../test.module';
import { AccesComponent } from 'app/entities/acces/acces.component';
import { AccesService } from 'app/entities/acces/acces.service';
import { Acces } from 'app/shared/model/acces.model';

describe('Component Tests', () => {
    describe('Acces Management Component', () => {
        let comp: AccesComponent;
        let fixture: ComponentFixture<AccesComponent>;
        let service: AccesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BadgeAppTestModule],
                declarations: [AccesComponent],
                providers: []
            })
                .overrideTemplate(AccesComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AccesComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AccesService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Acces(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.acces[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
