/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { BadgeAppTestModule } from '../../../test.module';
import { AccesUpdateComponent } from 'app/entities/acces/acces-update.component';
import { AccesService } from 'app/entities/acces/acces.service';
import { Acces } from 'app/shared/model/acces.model';

describe('Component Tests', () => {
    describe('Acces Management Update Component', () => {
        let comp: AccesUpdateComponent;
        let fixture: ComponentFixture<AccesUpdateComponent>;
        let service: AccesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BadgeAppTestModule],
                declarations: [AccesUpdateComponent]
            })
                .overrideTemplate(AccesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AccesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AccesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Acces(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.acces = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Acces();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.acces = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
