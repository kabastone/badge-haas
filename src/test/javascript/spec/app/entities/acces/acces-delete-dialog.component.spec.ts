/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BadgeAppTestModule } from '../../../test.module';
import { AccesDeleteDialogComponent } from 'app/entities/acces/acces-delete-dialog.component';
import { AccesService } from 'app/entities/acces/acces.service';

describe('Component Tests', () => {
    describe('Acces Management Delete Component', () => {
        let comp: AccesDeleteDialogComponent;
        let fixture: ComponentFixture<AccesDeleteDialogComponent>;
        let service: AccesService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BadgeAppTestModule],
                declarations: [AccesDeleteDialogComponent]
            })
                .overrideTemplate(AccesDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AccesDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AccesService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
