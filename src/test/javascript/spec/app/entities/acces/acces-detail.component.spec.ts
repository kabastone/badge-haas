/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BadgeAppTestModule } from '../../../test.module';
import { AccesDetailComponent } from 'app/entities/acces/acces-detail.component';
import { Acces } from 'app/shared/model/acces.model';

describe('Component Tests', () => {
    describe('Acces Management Detail Component', () => {
        let comp: AccesDetailComponent;
        let fixture: ComponentFixture<AccesDetailComponent>;
        const route = ({ data: of({ acces: new Acces(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BadgeAppTestModule],
                declarations: [AccesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AccesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AccesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.acces).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
